/**
 *  \brief		Project: War Simulation
 *  \details	This is a simulation of the game of war.  It determines the
 *				average amount of turns in a war game.
 *  \author		Brandon Staab <bls114@zips.uakron.edu>
 *  \version	1.0.0
 *  \date		September 2017
 *  \copyright	COPYRIGHT (C) 2017 Brandon Staab (bls114) All rights reserved.
 */

#include "Machine.hpp"

#include "GameOptions.hpp"

#include <iomanip>
#include <iostream>


int Machine::run() {
	try {
		gameStats = std::make_shared<GameStats>("war-stats.txt");

		std::cout << std::fixed << std::setprecision(GameOptions::PRECISION);

		// Spawn enough threads to saturate the cpu
		std::size_t threadsToSpawn = std::thread::hardware_concurrency();
		if (threadsToSpawn == 0) threadsToSpawn = 1;

		// creat and run each game instance
		for (std::size_t i = 0; i < threadsToSpawn; ++i) {
			games.emplace_back(std::make_shared<Game>());
			threads.emplace_back(std::thread{&Game::run, games[i]});
		}

		// spawn the reporter thread
		threads.push_back(std::thread{&GameStats::reporter, gameStats});

		// wait for all the threads to finish
		for (auto& th : threads) th.join();

		std::cout << "The average length of war is " << gameStats->getAvg()
				  << " moves long." << std::endl;
	}

	catch (std::exception& e) {
		std::cerr << "Exception Caught: " << e.what() << '\n';

		return 1;
	}

	return 0;
}

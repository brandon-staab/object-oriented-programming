/**
 *  \brief		Project: War Simulation
 *  \details	This is a simulation of the game of war.  It determines the
 *				average amount of turns in a war game.
 *  \author		Brandon Staab <bls114@zips.uakron.edu>
 *  \version	1.0.0
 *  \date		September 2017
 *  \copyright	COPYRIGHT (C) 2017 Brandon Staab (bls114) All rights reserved.
 */

#include "pushTableOnHand.hpp"


void pushTableOnHand(Player* players, const bool player) {
	bool turn = !player;

	do {
		if (!players[turn].table.empty()) {
			players[player].hand.push(players[turn].table.front());
			players[turn].table.pop();
		}

		turn = !turn;
	} while (!(players[0].table.empty() && players[1].table.empty()));
}

/**
 *  \brief		Project: War Simulation
 *  \details	This is a simulation of the game of war.  It determines the
 *				average amount of turns in a war game.
 *  \author		Brandon Staab <bls114@zips.uakron.edu>
 *  \version	1.0.0
 *  \date		September 2017
 *  \copyright	COPYRIGHT (C) 2017 Brandon Staab (bls114) All rights reserved.
 */

#include "Game.hpp"

#include "GameOptions.hpp"
#include "pushTableOnHand.hpp"


Game::Game()
	: moves(0),
	  deck(GameOptions::IS_JOKERS ? createStandardDeck()
								  : createJokerlessDeck()),
	  players({{}, {}}),
	  loopClock(std::chrono::system_clock::now() + std::chrono::seconds(15)) {}


Game::~Game() {
	for (auto card : deck) {
		delete card;
	}
}


Game::Game(std::shared_ptr<Game> other)
	: moves(other->moves), players({{}, {}}), loopClock(other->loopClock) {
	players[0] = other->players[0];
	players[1] = other->players[1];
}


void Game::run() {
	do {
		step();
	} while (!gameStats->isSameAvg());
}


void Game::step() {
	do {
		reset();
		play();

		++players[winningPlayer()].wins;
	} while (std::chrono::system_clock::now() < loopClock);

	updateGameStats();
	loopClock = std::chrono::system_clock::now() + std::chrono::seconds(15);
}


void Game::play() {
	do {
		turn();
	} while (!gameOver());
}


void Game::turn() {
	int scoreDiff = players[0].hand.front()->getStrength() -
					players[1].hand.front()->getStrength();
	if (scoreDiff == 0) {
		duelTie();
	} else {
		duelWin(scoreDiff < 0);
	}

	++moves;
}


void Game::duelTie() {
	for (size_t i = 0; i < GameOptions::NUM_SACRIFICE; ++i) {
		players[0].table.push(players[0].hand.front());
		players[1].table.push(players[1].hand.front());

		players[0].hand.pop();
		players[1].hand.pop();

		if (gameOver()) break;
	}
}


void Game::duelWin(const bool player) {
	players[player].table.push(players[player].hand.front());
	players[player].table.push(players[!player].hand.front());

	players[!player].hand.pop();
	players[player].hand.pop();

	pushTableOnHand(players, player);
}


void Game::reset() {
	deck << players[0].hand << players[1].hand << players[0].table
		 << players[1].table;
	shuffle(deck);
	dealCards(deck, players[0].hand, deck.size() / 2);
	dealCards(deck, players[1].hand, deck.size());
}


void Game::updateGameStats() {
	gameStats->addMoves(moves);
	gameStats->addWins(players[0].wins + players[1].wins);
	gameStats->updateAvg();
}


bool Game::gameOver() const {
	return players[0].hand.empty() || players[1].hand.empty();
}


bool Game::winningPlayer() const {
	return players[1].hand.empty();
}

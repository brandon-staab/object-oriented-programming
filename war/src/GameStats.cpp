/**
 *  \brief		Project: War Simulation
 *  \details	This is a simulation of the game of war.  It determines the
 *				average amount of turns in a war game.
 *  \author		Brandon Staab <bls114@zips.uakron.edu>
 *  \version	1.0.0
 *  \date		September 2017
 *  \copyright	COPYRIGHT (C) 2017 Brandon Staab (bls114) All rights reserved.
 */

#include "GameStats.hpp"

#include "GameOptions.hpp"

#include <chrono>
#include <cmath>
#include <iostream>
#include <limits>
#include <stdexcept>
#include <thread>


GameStats::GameStats(const char* fileName)
	: totalGames(),
	  totalMoves(),
	  avgCards(-1.0),
	  lastAvgCards(),
	  file(fileName) {
	// Try to open a new one
	if (!file.is_open()) {
		file.open(fileName,
				  std::fstream::in | std::fstream::out | std::fstream::trunc);
	}

	// File can not be opened
	if (!file.is_open()) {
		throw std::runtime_error("Can't open file.");
	}

	load();
}


GameStats::~GameStats() {
	save();
	file.close();
}


void GameStats::addWins(const unsigned long long int numOfWins) {
	totalGames += numOfWins;
}


void GameStats::addMoves(const unsigned long long int amount) {
	totalMoves += amount;
}


double GameStats::getAvg() const {
	return avgCards.load();
}


constexpr int precision = pow(10, GameOptions::PRECISION);
bool GameStats::isSameAvg() const {
	return std::trunc(avgCards * precision) ==
		   std::trunc(lastAvgCards * precision);
}


void GameStats::updateAvg() {
	lastAvgCards = avgCards.load();
	avgCards = totalMoves / static_cast<long double>(totalGames);
}


void GameStats::load() {
	bool badInput = false;
	std::string line;

	try {
		std::getline(file, line);
		if (line.find("totalGames:", 0) == 0) {
			totalGames = std::stoull(line.substr(11));
		} else {
			badInput = true;
		}

		std::getline(file, line);
		if (line.find("totalMoves:", 0) == 0) {
			totalMoves = std::stoull(line.substr(11));
		} else {
			badInput = true;
		}
	}

	catch (const std::invalid_argument& ia) {
		std::cerr << "Invalid argument: " << ia.what() << '\n';
		badInput = true;
	}

	catch (const std::out_of_range& oor) {
		std::cerr << "Out of Range error: " << oor.what() << '\n';
		badInput = true;
	}

	if (badInput) {
		totalGames = 0;
		totalMoves = 0;
		file.clear();
		save();
	} else {
		std::cout << "Successfully Loaded Save: totalGames: " << totalGames
				  << "\ttotalMoves: " << totalMoves << std::endl;
	}

	updateAvg();
}


void GameStats::save() {
	file.ignore(std::numeric_limits<std::streamsize>::max(),
				std::char_traits<wchar_t>::eof());
	file.clear();
	file.seekp(0);
	file << "totalGames:" << totalGames << "\ntotalMoves:" << totalMoves;
	file.flush();
}


void GameStats::reporter() {
	do {
		std::cout << "Current Average: " << getAvg() << '\n';
		save();
		std::this_thread::sleep_until(std::chrono::system_clock::now() +
									  std::chrono::seconds(30));
	} while (!isSameAvg());
}

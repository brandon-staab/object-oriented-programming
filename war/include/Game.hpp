/**
 *  \brief		Project: War Simulation
 *  \details	This is a simulation of the game of war.  It determines the
 *				average amount of turns in a war game.
 *  \author		Brandon Staab <bls114@zips.uakron.edu>
 *  \version	1.0.0
 *  \date		September 2017
 *  \copyright	COPYRIGHT (C) 2017 Brandon Staab (bls114) All rights reserved.
 */

#ifndef GUARD_Game
#define GUARD_Game

#include "GameStats.hpp"
#include "Player.hpp"

#include "Deck.hpp"

#include <chrono>
#include <memory>


class Game {
   public:
	Game();
	~Game();
	Game(std::shared_ptr<Game> other);

	void run();

   private:
	unsigned long long int moves;
	Deck deck;
	Player players[2];
	std::chrono::time_point<std::chrono::system_clock> loopClock;


	void step();
	void play();
	void turn();
	void duelTie();
	void duelWin(const bool player);

	void reset();

	void updateGameStats();
	bool winningPlayer() const;
	bool gameOver() const;
};

#endif

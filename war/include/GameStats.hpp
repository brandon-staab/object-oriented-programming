/**
 *  \brief		Project: War Simulation
 *  \details	This is a simulation of the game of war.  It determines the
 *				average amount of turns in a war game.
 *  \author		Brandon Staab <bls114@zips.uakron.edu>
 *  \version	1.0.0
 *  \date		September 2017
 *  \copyright	COPYRIGHT (C) 2017 Brandon Staab (bls114) All rights reserved.
 */

#ifndef GUARD_GameStats
#define GUARD_GameStats

#include "Card.hpp"

#include <atomic>
#include <fstream>
#include <memory>


class GameStats {
   public:
	GameStats(const char* fileName);
	~GameStats();

	void addWins(const unsigned long long int numOfWins);
	void addMoves(const unsigned long long int amount);

	double getAvg() const;
	bool isSameAvg() const;
	void updateAvg();

	void reporter();

   private:
	std::atomic_ullong totalGames;
	std::atomic_ullong totalMoves;
	std::atomic<double> avgCards;
	std::atomic<double> lastAvgCards;
	std::fstream file;

	void load();
	void save();
};

extern std::shared_ptr<GameStats> gameStats;

#endif

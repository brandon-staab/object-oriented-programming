/**
 *  \brief		Project: War Simulation
 *  \details	This is a simulation of the game of war.  It determines the
 *				average amount of turns in a war game.
 *  \author		Brandon Staab <bls114@zips.uakron.edu>
 *  \version	1.0.0
 *  \date		September 2017
 *  \copyright	COPYRIGHT (C) 2017 Brandon Staab (bls114) All rights reserved.
 */

#ifndef GUARD_Machine
#define GUARD_Machine

#include "Game.hpp"

#include <thread>


class Machine {
   public:
	int run();

   private:
	std::vector<std::shared_ptr<Game>> games;
	std::vector<std::thread> threads;
};

#endif

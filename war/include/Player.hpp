/**
 *  \brief		Project: War Simulation
 *  \details	This is a simulation of the game of war.  It determines the
 *				average amount of turns in a war game.
 *  \author		Brandon Staab <bls114@zips.uakron.edu>
 *  \version	1.0.0
 *  \date		September 2017
 *  \copyright	COPYRIGHT (C) 2017 Brandon Staab (bls114) All rights reserved.
 */

#ifndef GUARD_Player
#define GUARD_Player

#include "Card.hpp"
#include "Hand.hpp"

#include <queue>


struct Player {
	Player();
	~Player();

	unsigned long long int wins;
	Hand hand;
	Hand table;
};

#endif

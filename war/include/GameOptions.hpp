/**
 *  \brief		Project: War Simulation
 *  \details	This is a simulation of the game of war.  It determines the
 *				average amount of turns in a war game.
 *  \author		Brandon Staab <bls114@zips.uakron.edu>
 *  \version	1.0.0
 *  \date		September 2017
 *  \copyright	COPYRIGHT (C) 2017 Brandon Staab (bls114) All rights reserved.
 */

#ifndef GUARD_GameOptions
#define GUARD_GameOptions


struct GameOptions {
	GameOptions() = delete;

	static const int PRECISION = 4;
	static const bool IS_JOKERS{true};
	static const bool NEGOTIABLE_SACRIFICE{false};  // TODO: imp
	static const unsigned int NUM_SACRIFICE = 2;
};

#endif

/**
 *  \brief		JSON
 *  \details	This is part of the json library
 *  \author		Brandon Staab <bls114@zips.uakron.edu>
 *  \version	1.0.0
 *  \date		November 2017
 *  \copyright	COPYRIGHT (C) 2017 Brandon Staab (bls114) All rights reserved.
 */


#ifndef _JSON_
#define _JSON_

#include <map>
#include <memory>
#include <string>
#include <vector>


struct Value {
	virtual ~Value();

	virtual void print() const = 0;
};


struct String : public Value, public std::string {
	using std::string::string;

	void print() const final;
};


struct Number : public Value {
	Number(double value);

	void print() const final;
	double value;
};


struct Object : public Value {
	~Object();

	void print() const final;
	void insert(String* key, Value* value);

	std::map<String*, Value*> fields;
};


struct Array : public Value, public std::vector<Value*> {
	using std::vector<Value*>::vector;
	~Array() override;

	void print() const final;
};


struct Boolean : public Value {
	Boolean(bool value);

	void print() const final;
	bool value;
};


struct Null : public Value {
	void print() const final;
};


/*** Non-Member Functions *****************************************************/
std::unique_ptr<Value> parse(const std::string& str);
void print(const std::unique_ptr<Value> value);

#endif

/**
 *  \brief		JSON
 *  \details	This is part of the json library
 *  \author		Brandon Staab <bls114@zips.uakron.edu>
 *  \version	1.0.0
 *  \date		November 2017
 *  \copyright	COPYRIGHT (C) 2017 Brandon Staab (bls114) All rights reserved.
 *							  2017 Andrew Sutton
 *							  https://gitlab.com/andrew.n.sutton/oop-1/blob/master/oop/json.cpp
 */


#include "json.hpp"

#include <cassert>
#include <iostream>
#include <sstream>


namespace {
/// Returns true if at the end of the file.
bool is_eof(const char* first, const char* last) {
	return first == last;
}

/// Returns true if *first is a punctuation character.
bool is_punctuation(const char* first, const char* last) {
	if (is_eof(first, last)) {
		return false;
	}

	switch (*first) {
		default:
			return false;
		case '[':
		case ']':
		case '{':
		case '}':
		case ':':
		case ',':
			return true;
	}
}

/// Returns true if *first is a space character.
bool is_space(const char* first, const char* last) {
	if (is_eof(first, last)) {
		return false;
	}

	return std::isspace(*first);
}

/// Returns true if first delimits two words.
bool is_delimiter(const char* first, const char* last) {
	return is_eof(first, last) || is_space(first, last) ||
		   is_punctuation(first, last);
}

bool match_literal(const char*& first, const char* last, const char* word) {
	const char* word_pos = word;
	while (!is_eof(first, last) && *word_pos != '\0' && *first == *word_pos) {
		++first;
		++word_pos;
	}

	// make sure word_pos is at the end of a c-string
	if (*word_pos != '\0') {
		throw std::runtime_error("invalid literal");
	}

	// make sure we stopped at a delimiter
	if (!is_delimiter(first, last)) {
		throw std::runtime_error("invalid literal");
	}

	return true;
}
}


namespace sentinel {
/// Skip whitespace.
void skip_space(const char*& first, const char* last) {
	while (is_space(first, last)) {
		++first;
	}
}

/// A helper class that can be used to skip whitespace around a term.
///
/// This is called a guard or sentinel class. It executes code *around*
/// a region of code. Also called an RAII helper.
///
/// RAII -- Resource Acquisition Is Initialization
struct space_guard {
	space_guard(const char*& f, const char* l) : first(f), last(l) {
		skip_space(first, last);
	}

	~space_guard() { skip_space(first, last); }

	const char*& first;
	const char* last;
};
}


// This is where the parsing of each type of JSON object is done
namespace parsing {
/// Parse a value from a range of characters.
Value* parse_value(const char*& first, const char* last);


/// Parse the true value.
Boolean* parse_true(const char*& first, const char* last) {
	match_literal(first, last, "true");

	return new Boolean(true);
}


/// Parse the false value.
Boolean* parse_false(const char*& first, const char* last) {
	match_literal(first, last, "false");

	return new Boolean(false);
}


/// Parse the null value.
Null* parse_null(const char*& first, const char* last) {
	match_literal(first, last, "null");

	return new Null();
}


/// Parse a string value.
String* parse_string(const char*& first, const char* last) {
	const char* start = first++;

	while (!is_eof(first, last) && *first != '"') {
		if (*first == '\\') {  // Skip escaped char
			++first;
		}
		++first;
	}
	assert(*first == '"');
	++first;

	return new String(start, first);
}


/// Parse a numeric value.
Number* parse_number(const char*& first, const char* last) {
	const char* start = first++;
	while (!is_eof(first, last) && !is_delimiter(first, last)) {
		++first;
	}

	// Try to interpret the number as a string.
	double num;
	if (!(std::stringstream{std::string{start, first}} >> num)) {
		throw std::runtime_error("invalid number");
	}

	return new Number(num);
}


/// Parse an array.
Array* parse_array(const char*& first, const char* last) {
	// Pre-allocate the array.
	Array* arr = new Array();

	++first;
	sentinel::space_guard remove_space(first, last);

	// Special case: an empty array.
	if (*first == ']') {
		++first;
		return arr;
	}

	// Parse a sequence of comma delimited values.
	while (true) {
		// Parse and save the value.
		arr->emplace_back(parse_value(first, last));

		if (is_eof(first, last)) {
			throw std::runtime_error(
				"invalid array");  // Should find ] when is_eof() is true
		}

		// check for another value
		if (*first == ',') {
			++first;
			continue;
		}

		// check if we found the end of the array
		if (*first == ']') break;
	}
	assert(*first == ']');
	++first;

	return arr;
}


/// Parse a key. This simply parses a string, ensuring that surrounding
/// spaces are skipped.
String* parse_key(const char*& first, const char* last) {
	sentinel::space_guard remove_space(first, last);
	return parse_string(first, last);
}


/// Parse an object
Object* parse_object(const char*& first, const char* last) {
	// Pre-allocate the objet.
	Object* obj = new Object();

	++first;
	sentinel::space_guard remove_space(first, last);

	// Special case: an empty object.
	if (*first == '}') {
		++first;
		return obj;
	}

	// Parse a sequence of comma delimited values.
	while (true) {
		// Parse the key.
		String* key = parse_key(first, last);

		if (is_eof(first, last)) {
			throw std::runtime_error("invalid object");
		}
		if (*first != ':') {
			throw std::runtime_error("got: " + std::string(first) +
									 " expected ':'");
		}

		obj->insert(key, parse_value(++first, last));

		if (is_eof(first, last)) {
			throw std::runtime_error("invalid array of fields");  // should find
																  // } when
																  // is_eof() is
																  // true
		}

		// check for another field
		if (*first == ',') {
			++first;
			continue;
		}

		// check if we found the end of the object
		if (*first == '}') {
			break;
		}
	}
	assert(*first == '}');
	++first;

	return obj;
}


/// The value parsed depends on the current character.
Value* parse_value(const char*& first, const char* last) {
	if (is_eof(first, last)) {
		return nullptr;
	}

	// This is a guard or sentinel.
	sentinel::space_guard remove_space(first, last);

	switch (*first) {
		case 't':
			return parse_true(first, last);
		case 'f':
			return parse_false(first, last);
		case 'n':
			return parse_null(first, last);
		case '"':
			return parse_string(first, last);
		case '[':
			return parse_array(first, last);
		case '{':
			return parse_object(first, last);
		case '0':
		case '1':
		case '2':
		case '3':
		case '4':
		case '5':
		case '6':
		case '7':
		case '8':
		case '9':
		case '-':
			return parse_number(first, last);
		default:
			throw std::runtime_error("invalid value");
	}
}
}


/// parses string and returns JSON AST wrapped as a unique_ptr
std::unique_ptr<Value> parse(const std::string& str) {
	const char* first = str.c_str();
	const char* last = first + str.size();

	return std::unique_ptr<Value>(parsing::parse_value(first, last));
}


/// prints JSON AST
void print(const std::unique_ptr<Value> value) {
	if (value != nullptr) {
		value->print();
	}
}

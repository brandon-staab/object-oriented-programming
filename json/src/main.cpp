/**
 *  \brief		JSON
 *  \details	This is part of the json library
 *  \author		Brandon Staab <bls114@zips.uakron.edu>
 *  \version	1.0.0
 *  \date		November 2017
 *  \copyright	COPYRIGHT (C) 2017 Brandon Staab (bls114) All rights reserved.
 */


#include "json.hpp"

#include <iostream>
#include <iterator>
#include <string>


int main() {
	// https://stackoverflow.com/questions/2602013/read-whole-ascii-file-into-c-stdstring
	std::string input((std::istreambuf_iterator<char>(std::cin)),
					  std::istreambuf_iterator<char>());

	std::cout << '\n';
	print(parse(input));
	std::cout << '\n';

	return 0;
}

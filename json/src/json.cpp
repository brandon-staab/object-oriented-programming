/**
 *  \brief		JSON
 *  \details	This is part of the json library
 *  \author		Brandon Staab <bls114@zips.uakron.edu>
 *  \version	1.0.0
 *  \date		November 2017
 *  \copyright	COPYRIGHT (C) 2017 Brandon Staab (bls114) All rights reserved.
 */


#include "json.hpp"

#include <cassert>
#include <iostream>


/*** Value ********************************************************************/
Value::~Value() {}


/** String ********************************************************************/
void String::print() const {
	std::cout << '"' << *this << '"';
}


/*** Number *******************************************************************/
Number::Number(double value) : value(value){};


void Number::print() const {
	std::cout << value;
}


/*** Object *******************************************************************/
Object::~Object() {
	for (auto field : fields) {
		delete field.first;
		delete field.second;
	}
}

void Object::print() const {
	std::cout << '{';

	auto iter = fields.begin();
	while (iter != fields.end()) {
		if (iter->first == nullptr || iter->second == nullptr) {
			throw std::invalid_argument("field in object is null");
		}
		iter->first->print();
		std::cout << " : ";
		iter->second->print();
		if (++iter != fields.end()) {
			std::cout << ", ";
		}
	}

	std::cout << '}';
}


void Object::insert(String* key, Value* value) {
	assert(key != nullptr);
	assert(value != nullptr);
	fields.emplace(key, value);
}


/*** Array ********************************************************************/
Array::~Array() {
	for (Value* val : *this) {
		delete val;
	};
}


void Array::print() const {
	std::cout << '[';

	auto iter = begin();
	while (iter != end()) {
		if (*iter == nullptr) {
			throw std::invalid_argument("element in array is null");
		}
		(*iter)->print();
		if (++iter != end()) {
			std::cout << ", ";
		}
	}

	std::cout << ']';
}


/*** Boolean ******************************************************************/
Boolean::Boolean(bool value) : value(value){};


void Boolean::print() const {
	if (value) {
		std::cout << "true";
	} else {
		std::cout << "false";
	}
}


/*** Null *********************************************************************/
void Null::print() const {
	std::cout << "null";
}

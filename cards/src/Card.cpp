/**
 *  \brief		Card class
 *  \details	This is part of the cards library
 *  \author		Brandon Staab <bls114@zips.uakron.edu>
 *  \version	1.0.0
 *  \date		September 2017
 *  \copyright	COPYRIGHT (C) 2017 Brandon Staab (bls114) All rights reserved.
 */

#include "Card.hpp"

#include "CardOptions.hpp"

#include <cassert>
#include <functional>
#include <iostream>


namespace {
constexpr Color suitToColor(const Suit suit) {
	if (suit == Clubs || suit == Spades) {
		return Black;
	} else {
		return Red;
	}
}
}

/*** Card *********************************************************************/
Card::~Card() {}


/*** Null Card ****************************************************************/
// Card
CardType NullCard::getType() const {
	return NULL_CARD;
}


unsigned char NullCard::getValue() const {
	return 0x00;
}


/*** PlayingCard **************************************************************/
PlayingCard::~PlayingCard() {}


/*** FaceCard *****************************************************************/
FaceCard::FaceCard(const Rank rank, const Suit suit) : rank(rank), suit(suit) {
	assert(0 <= rank && rank <= RANK_MAX);
	assert(0 <= suit && suit <= SUIT_MAX);
}

// Card
CardType FaceCard::getType() const {
	return FACE_CARD;
}


unsigned char FaceCard::getValue() const {
	return 0x40 | (getRank() << 2) | getSuit();
}


// FaceCard
int FaceCard::getStrength() const {
	return rank;
}


Color FaceCard::getColor() const {
	return suitToColor(getSuit());
}


Rank FaceCard::getRank() const {
	assert(0 <= rank);
	assert(rank <= RANK_MAX);

	return rank;
}


Suit FaceCard::getSuit() const {
	assert(0 <= suit);
	assert(suit <= SUIT_MAX);

	return suit;
}


/*** TrumpCard ****************************************************************/
TrumpCard::TrumpCard(const Color color, const Special special)
	: color(color), special(special) {
	assert(0 <= color && color <= COLOR_MAX);
	assert(0 <= special && special <= SPECIAL_MAX);
}


// Card
CardType TrumpCard::getType() const {
	return TRUMP_CARD;
}


unsigned char TrumpCard::getValue() const {
	return 0x80 | (getSpecial() << 1) | getColor();
}


// TrumpCard
int TrumpCard::getStrength() const {
	if (getSpecial() == Joker) {
		return CardOptions::JOKE_STRENGTH;
	} else {
		assert(false);  // Specail not defined
	}
}


Color TrumpCard::getColor() const {
	return color;
}


Special TrumpCard::getSpecial() const {
	assert(0 <= special);
	assert(special <= SPECIAL_MAX);

	return special;
}


/*** Equality comparison ******************************************************/
bool operator==(const Card& lhs, const Card& rhs) {
	return lhs.getValue() == rhs.getValue();
}


bool operator!=(const Card& lhs, const Card& rhs) {
	return !(lhs == rhs);
}


/*** Ordering *****************************************************************/
bool operator<(const Card& lhs, const Card& rhs) {
	return (lhs.getValue() - rhs.getValue()) > 0;
}


bool operator>(const Card& lhs, const Card& rhs) {
	return rhs < lhs;
}


bool operator<=(const Card& lhs, const Card& rhs) {
	return !(rhs < lhs);
}


bool operator>=(const Card& lhs, const Card& rhs) {
	return !(lhs < rhs);
}


/*** Stream *******************************************************************/
std::ostream& operator<<(std::ostream& os, const Color& color) {
	switch (color) {
		case Black:
			os << 'B';
			break;
		case Red:
			os << 'R';
			break;
		default:
			assert(false);  // Color not defined
			break;
	}

	return os;
}


std::ostream& operator<<(std::ostream& os, const Suit& suit) {
	switch (suit) {
		case Clubs:
			os << 'C';
			break;
		case Diamonds:
			os << 'D';
			break;
		case Hearts:
			os << 'H';
			break;
		case Spades:
			os << 'S';
			break;
		default:
			assert(false);  // Suit not defined
			break;
	}

	return os;
}


std::ostream& operator<<(std::ostream& os, const Rank& rank) {
	switch (rank) {
		case Ace:
			os << 'A';
			break;
		case Two:
			os << '2';
			break;
		case Three:
			os << '3';
			break;
		case Four:
			os << '4';
			break;
		case Five:
			os << '5';
			break;
		case Six:
			os << '6';
			break;
		case Seven:
			os << '7';
			break;
		case Eight:
			os << '8';
			break;
		case Nine:
			os << '9';
			break;
		case Ten:
			os << 'T';
			break;
		case Jack:
			os << 'j';
			break;
		case Queen:
			os << 'Q';
			break;
		case King:
			os << 'K';
			break;
		case Knight:
			os << 'k';
			break;
		default:
			assert(false);  // Rank not defined
			break;
	}

	return os;
}


std::ostream& operator<<(std::ostream& os, const Special& special) {
	switch (special) {
		case Joker:
			os << 'J';
			break;
		default:
			assert(false);  // Special not defined
			break;
	}

	return os;
}


std::ostream& operator<<(std::ostream& os, const Card* card) {
	switch (card->getType()) {
		case NULL_CARD:
			os << *static_cast<const NullCard*>(card);
			break;
		case FACE_CARD:
			os << *static_cast<const FaceCard*>(card);
			break;
		case TRUMP_CARD:
			os << *static_cast<const TrumpCard*>(card);
			break;
	}

	return os;
}


std::ostream& operator<<(std::ostream& os, const NullCard& card) {
	return os << "--";
}


std::ostream& operator<<(std::ostream& os, const FaceCard& card) {
	return os << card.getRank() << card.getSuit();
}


std::ostream& operator<<(std::ostream& os, const TrumpCard& card) {
	return os << card.getColor() << card.getSpecial();
}

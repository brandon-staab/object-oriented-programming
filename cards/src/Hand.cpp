/**
 *  \brief		Hand class
 *  \details	This is part of the cards library
 *  \author		Brandon Staab <bls114@zips.uakron.edu>
 *  \version	1.0.0
 *  \date		September 2017
 *  \copyright	COPYRIGHT (C) 2017 Brandon Staab (bls114) All rights reserved.
 */

#include "Hand.hpp"

#include <cassert>


void dealCards(Deck& deck, Hand& hand, const std::size_t numOfCards) {
	assert(deck.size() >= numOfCards);
	for (std::size_t i = 0; i < numOfCards; ++i) {
		hand.push(deck.back());
		deck.pop_back();
	}
}


void returnToDiscard(Discard& discard, Hand& hand) {
	while (!hand.empty()) {
		discard.push_front(hand.front());
		hand.pop();
	}
}


Deck& operator<<(Deck& deck, Hand& hand) {
	while (!hand.empty()) {
		deck.push_back(hand.front());
		hand.pop();
	}

	return deck;
}

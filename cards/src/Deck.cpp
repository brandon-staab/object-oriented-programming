/**
 *  \brief		Deck class
 *  \details	This is part of the cards library
 *  \author		Brandon Staab <bls114@zips.uakron.edu>
 *  \version	1.0.0
 *  \date		September 2017
 *  \copyright	COPYRIGHT (C) 2017 Brandon Staab (bls114) All rights reserved.
 */

#include "Deck.hpp"

#include "getRandEngine.hpp"

#include <algorithm>
#include <iostream>


/*** Deck Creation ************************************************************/
Deck createJokerlessDeck() {
	return Deck{new FaceCard{Ace, Clubs},	new FaceCard{Ace, Diamonds},
				new FaceCard{Ace, Hearts},   new FaceCard{Ace, Spades},
				new FaceCard{Two, Clubs},	new FaceCard{Two, Diamonds},
				new FaceCard{Two, Hearts},   new FaceCard{Two, Spades},
				new FaceCard{Three, Clubs},  new FaceCard{Three, Diamonds},
				new FaceCard{Three, Hearts}, new FaceCard{Three, Spades},
				new FaceCard{Four, Clubs},   new FaceCard{Four, Diamonds},
				new FaceCard{Four, Hearts},  new FaceCard{Four, Spades},
				new FaceCard{Five, Clubs},   new FaceCard{Five, Diamonds},
				new FaceCard{Five, Hearts},  new FaceCard{Five, Spades},
				new FaceCard{Six, Clubs},	new FaceCard{Six, Diamonds},
				new FaceCard{Six, Hearts},   new FaceCard{Six, Spades},
				new FaceCard{Seven, Clubs},  new FaceCard{Seven, Diamonds},
				new FaceCard{Seven, Hearts}, new FaceCard{Seven, Spades},
				new FaceCard{Eight, Clubs},  new FaceCard{Eight, Diamonds},
				new FaceCard{Eight, Hearts}, new FaceCard{Eight, Spades},
				new FaceCard{Nine, Clubs},   new FaceCard{Nine, Diamonds},
				new FaceCard{Nine, Hearts},  new FaceCard{Nine, Spades},
				new FaceCard{Ten, Clubs},	new FaceCard{Ten, Diamonds},
				new FaceCard{Ten, Hearts},   new FaceCard{Ten, Spades},
				new FaceCard{Jack, Clubs},   new FaceCard{Jack, Diamonds},
				new FaceCard{Jack, Hearts},  new FaceCard{Jack, Spades},
				new FaceCard{Queen, Clubs},  new FaceCard{Queen, Diamonds},
				new FaceCard{Queen, Hearts}, new FaceCard{Queen, Spades},
				new FaceCard{King, Clubs},   new FaceCard{King, Diamonds},
				new FaceCard{King, Hearts},  new FaceCard{King, Spades}};
}


Deck createStandardDeck() {
	return Deck{new FaceCard{Ace, Clubs},	new FaceCard{Ace, Diamonds},
				new FaceCard{Ace, Hearts},   new FaceCard{Ace, Spades},
				new FaceCard{Two, Clubs},	new FaceCard{Two, Diamonds},
				new FaceCard{Two, Hearts},   new FaceCard{Two, Spades},
				new FaceCard{Three, Clubs},  new FaceCard{Three, Diamonds},
				new FaceCard{Three, Hearts}, new FaceCard{Three, Spades},
				new FaceCard{Four, Clubs},   new FaceCard{Four, Diamonds},
				new FaceCard{Four, Hearts},  new FaceCard{Four, Spades},
				new FaceCard{Five, Clubs},   new FaceCard{Five, Diamonds},
				new FaceCard{Five, Hearts},  new FaceCard{Five, Spades},
				new FaceCard{Six, Clubs},	new FaceCard{Six, Diamonds},
				new FaceCard{Six, Hearts},   new FaceCard{Six, Spades},
				new FaceCard{Seven, Clubs},  new FaceCard{Seven, Diamonds},
				new FaceCard{Seven, Hearts}, new FaceCard{Seven, Spades},
				new FaceCard{Eight, Clubs},  new FaceCard{Eight, Diamonds},
				new FaceCard{Eight, Hearts}, new FaceCard{Eight, Spades},
				new FaceCard{Nine, Clubs},   new FaceCard{Nine, Diamonds},
				new FaceCard{Nine, Hearts},  new FaceCard{Nine, Spades},
				new FaceCard{Ten, Clubs},	new FaceCard{Ten, Diamonds},
				new FaceCard{Ten, Hearts},   new FaceCard{Ten, Spades},
				new FaceCard{Jack, Clubs},   new FaceCard{Jack, Diamonds},
				new FaceCard{Jack, Hearts},  new FaceCard{Jack, Spades},
				new FaceCard{Queen, Clubs},  new FaceCard{Queen, Diamonds},
				new FaceCard{Queen, Hearts}, new FaceCard{Queen, Spades},
				new FaceCard{King, Clubs},   new FaceCard{King, Diamonds},
				new FaceCard{King, Hearts},  new FaceCard{King, Spades},
				new TrumpCard{Black, Joker}, new TrumpCard{Red, Joker}};
}


/*** General*******************************************************************/
void print(const Deck& deck) {
	std::cout << deck << '\n';
}


/*** Algorithms ***************************************************************/
Deck& shuffle(Deck& deck) {
	std::shuffle(deck.begin(), deck.end(), getRandEngine());
	return deck;
}


Deck& sort(Deck& deck) {
	std::sort(deck.begin(), deck.end());
	return deck;
}


/*** Stream *******************************************************************/
std::ostream& operator<<(std::ostream& os, const Deck& deck) {
	for (auto card : deck) {
		std::cout << card << ' ';
	}

	return os;
}

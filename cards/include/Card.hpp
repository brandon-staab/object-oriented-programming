/**
 *  \brief		Card class
 *  \details	This is part of the cards library
 *  \author		Brandon Staab <bls114@zips.uakron.edu>
 *  \version	1.0.0
 *  \date		September 2017
 *  \copyright	COPYRIGHT (C) 2017 Brandon Staab (bls114) All rights reserved.
 */

#ifndef GUARD_Card
#define GUARD_Card

#include <iosfwd>


enum CardType {
	NULL_CARD,
	FACE_CARD,
	TRUMP_CARD,

	CARDTYPE_MAX = TRUMP_CARD
};


enum Color {
	Black,
	Red,

	COLOR_MAX = Red
};


enum Special {
	Joker,

	SPECIAL_MAX = Joker
};


enum Suit {
	Clubs,
	Diamonds,
	Hearts,
	Spades,

	SUIT_MAX = Spades
};


enum Rank {
	Ace,
	Two,
	Three,
	Four,
	Five,
	Six,
	Seven,
	Eight,
	Nine,
	Ten,
	Jack,
	Knight,
	Queen,
	King,

	RANK_MAX = King
};


struct Card {
	virtual ~Card();

	virtual CardType getType() const = 0;
	virtual unsigned char getValue() const = 0;
};

struct NullCard : public Card {
	NullCard();

	/*** Card *****************************************************************/
	CardType getType() const final;
	unsigned char getValue() const final;
};

struct PlayingCard : public Card {
	virtual ~PlayingCard();

	virtual int getStrength() const = 0;
	virtual Color getColor() const = 0;
};


struct FaceCard : public PlayingCard {
	FaceCard(const Rank rank, const Suit suit);


	/*** Card *****************************************************************/
	CardType getType() const final;
	unsigned char getValue() const final;

	/*** PlayingCard **********************************************************/
	int getStrength() const final;
	Color getColor() const final;

	/*** FaceCard *************************************************************/
	Rank getRank() const;
	Suit getSuit() const;

   private:
	Rank rank;
	Suit suit;
};


struct TrumpCard : public PlayingCard {
	TrumpCard(const Color color, const Special special);


	/*** Card *****************************************************************/
	CardType getType() const final;
	unsigned char getValue() const final;

	/*** PlayingCard **********************************************************/
	int getStrength() const final;
	Color getColor() const final;

	/*** TrumpCard ************************************************************/
	Special getSpecial() const;

   private:
	Color color;
	Special special;
};


/*** Equality comparison ******************************************************/
bool operator==(const Card& lhs, const Card& rhs);
bool operator!=(const Card& lhs, const Card& rhs);


/*** Ordering *****************************************************************/
bool operator<(const Card& lhs, const Card& rhs);
bool operator>(const Card& lhs, const Card& rhs);
bool operator<=(const Card& lhs, const Card& rhs);
bool operator>=(const Card& lhs, const Card& rhs);


/*** Stream *******************************************************************/
std::ostream& operator<<(std::ostream& os, const Color& color);
std::ostream& operator<<(std::ostream& os, const Rank& rank);
std::ostream& operator<<(std::ostream& os, const Special& special);

std::ostream& operator<<(std::ostream& os, const Card* card);
std::ostream& operator<<(std::ostream& os, const NullCard& card);
std::ostream& operator<<(std::ostream& os, const FaceCard& card);
std::ostream& operator<<(std::ostream& os, const TrumpCard& card);

#endif

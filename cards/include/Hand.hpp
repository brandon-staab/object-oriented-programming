/**
 *  \brief		Deck class
 *  \details	This is part of the cards library
 *  \author		Brandon Staab <bls114@zips.uakron.edu>
 *  \version	1.0.0
 *  \date		September 2017
 *  \copyright	COPYRIGHT (C) 2017 Brandon Staab (bls114) All rights reserved.
 */

#ifndef GUARD_Hand
#define GUARD_Hand

#include "Deck.hpp"
#include "Discard.hpp"

#include <queue>


using Hand = std::queue<PlayingCard*>;


void dealCards(Deck& deck, Hand& hand, const std::size_t numOfCards);
void returnToDiscard(Discard& discard, Hand& hand);
Deck& operator<<(Deck& deck, Hand& hand);

#endif

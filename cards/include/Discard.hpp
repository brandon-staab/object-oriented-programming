/**
 *  \brief		Deck class
 *  \details	This is part of the cards library
 *  \author		Brandon Staab <bls114@zips.uakron.edu>
 *  \version	1.0.0
 *  \date		September 2017
 *  \copyright	COPYRIGHT (C) 2017 Brandon Staab (bls114) All rights reserved.
 */

#ifndef GUARD_Discard
#define GUARD_Discard

#include "Card.hpp"

#include <deque>


using Discard = std::deque<PlayingCard*>;


void returnToDiscard(Discard& discard, Card& card);

#endif

/**
 *  \brief		Deck class
 *  \details	This is part of the cards library
 *  \author		Brandon Staab <bls114@zips.uakron.edu>
 *  \version	1.0.0
 *  \date		September 2017
 *  \copyright	COPYRIGHT (C) 2017 Brandon Staab (bls114) All rights reserved.
 */

#ifndef GUARD_Deck
#define GUARD_Deck

#include "Card.hpp"

#include <vector>


using Deck = std::vector<PlayingCard*>;


/*** Deck Creation ************************************************************/
Deck createJokerlessDeck();
Deck createStandardDeck();

/*** General ******************************************************************/
void print(const Deck& deck);

/*** Algorithms ***************************************************************/
Deck& shuffle(Deck& deck);
Deck& sort(Deck& deck);

/*** Stream *******************************************************************/
std::ostream& operator<<(std::ostream& os, const Deck& deck);


#endif

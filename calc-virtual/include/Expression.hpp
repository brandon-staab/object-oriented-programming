#ifndef _Expression_
#define _Expression_

#include <iosfwd>


struct Expression {
	virtual ~Expression() = default;

	static void print_enclosed(std::ostream& os, const Expression* expr);

	virtual void print(std::ostream& os) const = 0;
	virtual int evaluate() const = 0;
	virtual Expression* clone() const = 0;
};


struct Integer : Expression {
	Integer(const int val) : val(val) {}

	void print(std::ostream& os) const override;
	int evaluate() const override;
	Integer* clone() const override;

	int val;
};


struct Negation : Expression {
	Negation(const Expression& that);
	~Negation() override;

	void print(std::ostream& os) const override;
	int evaluate() const override;
	Negation* clone() const override;

	Expression* expr;
};


struct BinaryOp : Expression {
	BinaryOp(Expression* lhs, Expression* rhs);
	BinaryOp(const BinaryOp& that);

	~BinaryOp() override;

	Expression* lhs;
	Expression* rhs;
};


struct Addition : BinaryOp {
	using BinaryOp::BinaryOp;

	void print(std::ostream& os) const override;
	int evaluate() const override;
	Addition* clone() const override;
};


struct Subtraction : BinaryOp {
	using BinaryOp::BinaryOp;

	void print(std::ostream& os) const override;
	int evaluate() const override;
	Subtraction* clone() const override;
};


struct Multiplication : BinaryOp {
	using BinaryOp::BinaryOp;

	void print(std::ostream& os) const override;
	int evaluate() const override;
	Multiplication* clone() const override;
};


struct Division : BinaryOp {
	using BinaryOp::BinaryOp;

	void print(std::ostream& os) const override;
	int evaluate() const override;
	Division* clone() const override;
};


#endif

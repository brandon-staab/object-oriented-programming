#include "Expression.hpp"

#include <iostream>


/*** Expression ***/
void Expression::print_enclosed(std::ostream& os, const Expression* expr) {
	os << '(';
	expr->print(os);
	os << ')';
}


/*** Integer ***/
void Integer::print(std::ostream& os) const {
	os << val;
}


int Integer::evaluate() const {
	return val;
}


Integer* Integer::clone() const {
	return new Integer(*this);
}


/*** Negation ***/
Negation::Negation(const Expression& that) : expr(that.clone()) {}
Negation::~Negation() {
	delete expr;
}


void Negation::print(std::ostream& os) const {
	os << '-' << expr->evaluate();
}


int Negation::evaluate() const {
	return -expr->evaluate();
}


Negation* Negation::clone() const {
	return new Negation(*this);
}


/*** BinaryOp ***/
BinaryOp::BinaryOp(Expression* lhs, Expression* rhs) : lhs(lhs), rhs(rhs) {}


BinaryOp::BinaryOp(const BinaryOp& that)
	: lhs(that.lhs->clone()), rhs(that.rhs->clone()) {}


BinaryOp::~BinaryOp() {
	delete lhs;
	delete rhs;
}


/*** Addition ***/
void Addition::print(std::ostream& os) const {
	print_enclosed(os, lhs);
	os << " + ";
	print_enclosed(os, rhs);
}


int Addition::evaluate() const {
	return lhs->evaluate() + rhs->evaluate();
}


Addition* Addition::clone() const {
	return new Addition(*this);
}


/*** Subtraction ***/
void Subtraction::print(std::ostream& os) const {
	print_enclosed(os, lhs);
	os << " - ";
	print_enclosed(os, rhs);
}
int Subtraction::evaluate() const {
	return lhs->evaluate() - rhs->evaluate();
}


Subtraction* Subtraction::clone() const {
	return new Subtraction(*this);
}


/*** Multiplication ***/
void Multiplication::print(std::ostream& os) const {
	print_enclosed(os, lhs);
	os << " * ";
	print_enclosed(os, rhs);
}


int Multiplication::evaluate() const {
	return lhs->evaluate() * rhs->evaluate();
}


Multiplication* Multiplication::clone() const {
	return new Multiplication(*this);
}


/*** Division ***/
void Division::print(std::ostream& os) const {
	print_enclosed(os, lhs);
	os << " / ";
	print_enclosed(os, rhs);
}


int Division::evaluate() const {
	return lhs->evaluate() / rhs->evaluate();
}


Division* Division::clone() const {
	return new Division(*this);
}

/**
 *  \brief		Expression
 *  \details	Virtual Functions
 *  \author		Brandon Staab <bls114@zips.uakron.edu>
 *  \version	1.0.0
 *  \date		November 2017
 *  \copyright	COPYRIGHT (C) 2017 Brandon Staab (bls114) All rights reserved.
 */


#include "Expression.hpp"

#include <iostream>


int main() {
	// (5 / 2) * (4 + 2)
	Expression* e =
		new Multiplication(new Division(new Integer(5), new Integer(2)),
						   new Addition(new Integer(4), new Integer(2)));

	std::cout << e << " = " << e->evaluate() << '\n';

	Expression* e2 = e->clone();

	delete e;

	std::cout << "e2 == " << e2->evaluate() << '\n';

	delete e2;
}

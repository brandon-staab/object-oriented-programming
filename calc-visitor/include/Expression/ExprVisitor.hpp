/**
 *  \brief		Expression
 *  \details	Visitor pattern
 *  \author		Brandon Staab <bls114@zips.uakron.edu>
 *  \version	1.0.0
 *  \date		November 2017
 *  \copyright	COPYRIGHT (C) 2017 Brandon Staab (bls114) All rights reserved.
 */

#ifndef _ExprVisitor_
#define _ExprVisitor_

#include "expr_fwd.hpp"


struct ExprVisitor {
	virtual void visit(Integer* expr) = 0;
	virtual void visit(Addition* expr) = 0;
	virtual void visit(Subtraction* expr) = 0;
	virtual void visit(Multiplication* expr) = 0;
	virtual void visit(Division* expr) = 0;
	virtual void visit(Negation* expr) = 0;
};

struct ConstExprVisitor {
	virtual void visit(const Integer* expr) = 0;
	virtual void visit(const Addition* expr) = 0;
	virtual void visit(const Subtraction* expr) = 0;
	virtual void visit(const Multiplication* expr) = 0;
	virtual void visit(const Division* expr) = 0;
	virtual void visit(const Negation* expr) = 0;
};

#endif

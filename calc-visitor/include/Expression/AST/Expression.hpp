/**
 *  \brief		Expression
 *  \details	Visitor pattern
 *  \author		Brandon Staab <bls114@zips.uakron.edu>
 *  \version	1.0.0
 *  \date		November 2017
 *  \copyright	COPYRIGHT (C) 2017 Brandon Staab (bls114) All rights reserved.
 */

#ifndef _Expression_
#define _Expression_

#include "../ExprVisitor.hpp"


struct Expression {
	virtual ~Expression() = default;

	virtual void accept(ExprVisitor& visitor) = 0;
	virtual void accept(ConstExprVisitor& visitor) const = 0;
};


struct BinaryOp : Expression {
	BinaryOp(Expression* lhs, Expression* rhs);
	~BinaryOp() override;

	Expression* lhs;
	Expression* rhs;
};

#endif

/**
 *  \brief		Expression
 *  \details	Visitor pattern
 *  \author		Brandon Staab <bls114@zips.uakron.edu>
 *  \version	1.0.0
 *  \date		November 2017
 *  \copyright	COPYRIGHT (C) 2017 Brandon Staab (bls114) All rights reserved.
 */

#ifndef _Expression_Integer_
#define _Expression_Integer_

#include "Expression.hpp"


struct Integer : Expression {
	Integer(int value) : value(value) {}

	void accept(ExprVisitor& visitor) final;
	void accept(ConstExprVisitor& visitor) const final;

	int value;
};

#endif

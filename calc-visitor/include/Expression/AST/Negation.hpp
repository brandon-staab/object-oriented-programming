/**
 *  \brief		Expression
 *  \details	Visitor pattern
 *  \author		Brandon Staab <bls114@zips.uakron.edu>
 *  \version	1.0.0
 *  \date		November 2017
 *  \copyright	COPYRIGHT (C) 2017 Brandon Staab (bls114) All rights reserved.
 */

#ifndef _Expression_Negation_
#define _Expression_Negation_

#include "Expression.hpp"


struct Negation : Expression {
	Negation(Expression* expr) : expr(expr) {}
	~Negation() final;

	void accept(ExprVisitor& visitor) final;
	void accept(ConstExprVisitor& visitor) const final;

	Expression* expr;
};

#endif

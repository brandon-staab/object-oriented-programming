/**
 *  \brief		Expression
 *  \details	Visitor pattern
 *  \author		Brandon Staab <bls114@zips.uakron.edu>
 *  \version	1.0.0
 *  \date		November 2017
 *  \copyright	COPYRIGHT (C) 2017 Brandon Staab (bls114) All rights reserved.
 */

#ifndef _expr_fwd_
#define _expr_fwd_

struct Expression;
struct Integer;
struct Addition;
struct Subtraction;
struct Multiplication;
struct Division;
struct Negation;

#endif

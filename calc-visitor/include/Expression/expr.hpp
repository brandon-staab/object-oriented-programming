/**
 *  \brief		Expression
 *  \details	Visitor pattern
 *  \author		Brandon Staab <bls114@zips.uakron.edu>
 *  \version	1.0.0
 *  \date		November 2017
 *  \copyright	COPYRIGHT (C) 2017 Brandon Staab (bls114) All rights reserved.
 */

#ifndef _expr_
#define _expr_

#include "expr_fwd.hpp"

#include <iosfwd>


// clone.cpp
Expression* clone(const Expression* that);

// evaluate.cpp
int evaluate(const Expression* expr);

// print.cpp
std::ostream& operator<<(std::ostream& os, const Expression* expr);
void print(std::ostream& os, const Expression* expr);


#include "AST/Integer.hpp"
#include "AST/Addition.hpp"
#include "AST/Subtraction.hpp"
#include "AST/Division.hpp"
#include "AST/Multiplication.hpp"
#include "AST/Negation.hpp"

#endif

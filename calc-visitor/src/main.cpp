/**
 *  \brief		Expression
 *  \details	Visitor pattern
 *  \author		Brandon Staab <bls114@zips.uakron.edu>
 *  \version	1.0.0
 *  \date		November 2017
 *  \copyright	COPYRIGHT (C) 2017 Brandon Staab (bls114) All rights reserved.
 */

#include "expr.hpp"

#include <iostream>


int main() {
	// (5 / 2) * (4 + 2)
	Expression* e =
		new Multiplication(new Division(new Integer(5), new Integer(2)),
						   new Addition(new Integer(4), new Integer(2)));

	std::cout << e << " = " << evaluate(e) << '\n';

	Expression* e2 = clone(e);

	delete e;

	std::cout << "e2 == " << evaluate(e2) << '\n';

	delete e2;
}

/**
 *  \brief		Expression
 *  \details	Visitor pattern
 *  \author		Brandon Staab <bls114@zips.uakron.edu>
 *  \version	1.0.0
 *  \date		November 2017
 *  \copyright	COPYRIGHT (C) 2017 Brandon Staab (bls114) All rights reserved.
 */

#include "expr.hpp"


Expression* clone(const Expression* that) {
	struct CloneVisitor : ConstExprVisitor {
		Expression* ret;

		void visit(const Integer* expr) final { ret = new Integer(*expr); }

		void visit(const Addition* expr) final {
			ret = new Addition{clone(expr->lhs), clone(expr->rhs)};
		}

		void visit(const Subtraction* expr) final {
			ret = new Subtraction{clone(expr->lhs), clone(expr->rhs)};
		}

		void visit(const Multiplication* expr) final {
			ret = new Multiplication{clone(expr->lhs), clone(expr->rhs)};
		}

		void visit(const Division* expr) final {
			ret = new Division{clone(expr->lhs), clone(expr->rhs)};
		}

		void visit(const Negation* expr) final {
			ret = new Negation{clone(expr->expr)};
		}
	} visitor;

	that->accept(visitor);

	return visitor.ret;
}

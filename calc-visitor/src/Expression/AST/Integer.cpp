/**
 *  \brief		Expression
 *  \details	Visitor pattern
 *  \author		Brandon Staab <bls114@zips.uakron.edu>
 *  \version	1.0.0
 *  \date		November 2017
 *  \copyright	COPYRIGHT (C) 2017 Brandon Staab (bls114) All rights reserved.
 */

#include "AST/Integer.hpp"


void Integer::accept(ExprVisitor& visitor) {
	visitor.visit(this);
}


void Integer::accept(ConstExprVisitor& visitor) const {
	visitor.visit(this);
}

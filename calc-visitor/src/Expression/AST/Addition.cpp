/**
 *  \brief		Expression
 *  \details	Visitor pattern
 *  \author		Brandon Staab <bls114@zips.uakron.edu>
 *  \version	1.0.0
 *  \date		November 2017
 *  \copyright	COPYRIGHT (C) 2017 Brandon Staab (bls114) All rights reserved.
 */

#include "AST/Addition.hpp"


void Addition::accept(ExprVisitor& visitor) {
	visitor.visit(this);
}


void Addition::accept(ConstExprVisitor& visitor) const {
	visitor.visit(this);
}

/**
 *  \brief		Expression
 *  \details	Visitor pattern
 *  \author		Brandon Staab <bls114@zips.uakron.edu>
 *  \version	1.0.0
 *  \date		November 2017
 *  \copyright	COPYRIGHT (C) 2017 Brandon Staab (bls114) All rights reserved.
 */

#include "expr.hpp"

#include <iostream>


std::ostream& operator<<(std::ostream& os, const Expression* expr) {
	print(os, expr);

	return os;
}


void print(std::ostream& os, const Expression* expr) {
	struct PrintVisitor : ConstExprVisitor {
		std::ostream& os;

		PrintVisitor(std::ostream& os) : os(os){};

		void visit(const Integer* expr) final { os << expr->value; }

		void visit(const Addition* expr) final {
			os << '(' << expr->lhs << '+' << expr->rhs << ')';
		}

		void visit(const Subtraction* expr) final {
			os << '(' << expr->lhs << '-' << expr->rhs << ')';
		}

		void visit(const Multiplication* expr) final {
			os << '(' << expr->lhs << '*' << expr->rhs << ')';
		}

		void visit(const Division* expr) final {
			os << '(' << expr->lhs << '/' << expr->rhs << ')';
		}

		void visit(const Negation* expr) final {
			os << "-(" << expr->expr << ')';
		}
	} visitor{os};

	expr->accept(visitor);
}

/**
 *  \brief		Expression
 *  \details	Visitor pattern
 *  \author		Brandon Staab <bls114@zips.uakron.edu>
 *  \version	1.0.0
 *  \date		November 2017
 *  \copyright	COPYRIGHT (C) 2017 Brandon Staab (bls114) All rights reserved.
 */

#include "expr.hpp"


int evaluate(const Expression* expr) {
	struct EvaluateVisitor : ConstExprVisitor {
		int ret;

		void visit(const Integer* expr) final { ret = expr->value; }

		void visit(const Addition* expr) final {
			ret = evaluate(expr->lhs) + evaluate(expr->rhs);
		}

		void visit(const Subtraction* expr) final {
			ret = evaluate(expr->lhs) - evaluate(expr->rhs);
		}

		void visit(const Multiplication* expr) final {
			ret = evaluate(expr->lhs) * evaluate(expr->rhs);
		}

		void visit(const Division* expr) final {
			ret = evaluate(expr->lhs) / evaluate(expr->rhs);
		}

		void visit(const Negation* expr) final { ret = -evaluate(expr->expr); }
	} visitor;

	expr->accept(visitor);

	return visitor.ret;
}

add_executable(
    Testing

    ${CMAKE_CURRENT_SOURCE_DIR}/src/main.cpp
)

target_include_directories(
	Testing PUBLIC

	include
)

target_link_libraries(
	Testing LINK_PUBLIC

	cards
	json
	utility
)

add_custom_command(
	TARGET Testing POST_BUILD
    COMMAND ${CMAKE_COMMAND} -E copy $<TARGET_FILE:Testing> ${CMAKE_CURRENT_SOURCE_DIR}/../build
)

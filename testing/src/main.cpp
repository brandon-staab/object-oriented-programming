#include "Deck.hpp"

#include <iostream>


int main() {
	std::cout << "Hello World" << '\n';

	Deck d = createStandardDeck();
	shuffle(d);
	sort(d);
	print(d);

	for (auto c : d) delete c;


	std::cout << "Goodbye!" << '\n';
	return 0;
}

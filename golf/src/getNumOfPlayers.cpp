/**
 *  \brief		Project: Golf
 *  \details	Implementation of Golf card game
 *  \author		Brandon Staab <bls114@zips.uakron.edu>
 *  \version	1.0.0
 *  \date		October 2017
 *  \copyright	COPYRIGHT (C) 2017 Brandon Staab (bls114) All rights reserved.
 */

#include "getNumOfPlayers.hpp"

#include "getConsoleInput.hpp"


std::size_t getNumOfPlayers() {
	std::size_t numOfPlayers;
	std::cout << "How many players? [2-8]" << std::endl;

get_numOfPlayers:
	numOfPlayers = getConsoleInput<std::size_t>();
	if (!(numOfPlayers >= 2 && numOfPlayers <= 8)) {
		std::cout << "Input must be an element of [2, 8]" << std::endl;
		goto get_numOfPlayers;
	}

	return numOfPlayers;
}

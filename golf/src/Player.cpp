/**
 *  \brief		Project: Golf
 *  \details	Implementation of Golf card game
 *  \author		Brandon Staab <bls114@zips.uakron.edu>
 *  \version	1.0.0
 *  \date		October 2017
 *  \copyright	COPYRIGHT (C) 2017 Brandon Staab (bls114) All rights reserved.
 */

#include "GameOptions.hpp"

#include "Player.hpp"

#include "getConsoleInput.hpp"

#include <iostream>
#include <numeric>
#include <utility>


Player::~Player() {
	for (std::size_t i = 0; i < GameOptions::NUM_CARDS_PER_PLAYER; ++i) {
		delete cards[i];
	}
}


void Player::peak() {
	std::cout << cards[0] << " " << cards[1] << std::endl;
}


int Player::getScore() const {
	return std::accumulate(
		cards, cards + GameOptions::NUM_CARDS_PER_PLAYER, 0,
		[](const int x, const PlayingCard* c) { return x + c->getStrength(); });
}


void Player::setCards(Hand& hand) {
	for (std::size_t i = 0; i < GameOptions::NUM_CARDS_PER_PLAYER; ++i) {
		cards[i] = hand.front();
		hand.pop();
	}
}


/*** Turn Types ***********************************************************/
void Player::skip() {
	std::cout << "Turn Skipped.\n";
}


void Player::drawFromDeck(Deck& deck) {
	std::swap(deck.back(), cards[getCardSwap()]);
}


void Player::drawFromDiscard(Discard& discard) {
	std::swap(discard.front(), cards[getCardSwap()]);
}


/*** Non-Member Functions *****************************************************/
std::size_t getCardSwap() {
	std::size_t move;
	std::cout << "What card do you want to replace? [1-"
			  << GameOptions::NUM_CARDS_PER_PLAYER << "]" << std::endl;

get_move:
	move = getConsoleInput<std::size_t>();
	if (!(move >= 1 && move <= GameOptions::NUM_CARDS_PER_PLAYER)) {
		std::cout << "Input must be an element of [1, "
				  << GameOptions::NUM_CARDS_PER_PLAYER << ']' << std::endl;
		goto get_move;
	}

	return move - 1;
}

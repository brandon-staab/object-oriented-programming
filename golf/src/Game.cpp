/**
 *  \brief		Project: Golf
 *  \details	Implementation of Golf card game
 *  \author		Brandon Staab <bls114@zips.uakron.edu>
 *  \version	1.0.0
 *  \date		October 2017
 *  \copyright	COPYRIGHT (C) 2017 Brandon Staab (bls114) All rights reserved.
 */

#include "GameOptions.hpp"

#include "Game.hpp"
#include "getConsoleInput.hpp"

#include <cassert>
#include <cctype>
#include <iostream>


Game::Game(const std::size_t numOfPlayers)
	: playing(true),
	  rounds(1),
	  currentPlayer(0),
	  maxPlayers(numOfPlayers),
	  deck(),
	  discard(),
	  players(std::make_unique<Player[]>(numOfPlayers)) {}


Game::~Game() {
	for (auto card : deck) {
		delete card;
	}

	for (auto card : discard) {
		delete card;
	}
}


void Game::reset() {
	rounds = 1;
	currentPlayer = 0;
	deck = createStandardDeck();
	discard.clear();

	shuffle(deck);

	assert(deck.size() > maxPlayers * GameOptions::NUM_CARDS_PER_PLAYER);
	Hand hand;
	for (std::size_t i = 0; i < maxPlayers; ++i) {
		dealCards(deck, hand, GameOptions::NUM_CARDS_PER_PLAYER);
		players[i].setCards(hand);
	}
}


int Game::run() {
	try {
		do {
			step();
		} while (isPlaying());
	} catch (std::exception& e) {
		std::cerr << "Exception Caught: " << e.what() << '\n';

		return 1;
	}

	return 0;
}


void Game::step() {
	reset();
	play();
	playing = askPlaying();
}


void Game::play() {
	round();

	displayScores();
	std::size_t winner = findWinner();
	if (winner == 0) {
		std::cout << "Tie!\n";
	} else {
		std::cout << "Player " << winner << " won the game.\n";
	}
}

void Game::round() {
	do {
		std::cout << "Round: " << rounds << '\n';
		turn();
		++rounds;
	} while (!gameOver());
}


void Game::turn() {
	assert(currentPlayer == 0);
	do {
		showCards();

		if (rounds == 1) {
			std::cout << "Front Cards: ";
			players[currentPlayer].peak();
		}

		askMove();
		std::cout << "Score: " << players[currentPlayer].getScore()
				  << "\n\n------------------------------\n\n";
		currentPlayer = (currentPlayer + 1) % maxPlayers;
	} while (currentPlayer != 0);
}


bool Game::isPlaying() const {
	return playing;
}


bool Game::gameOver() const {
	return rounds == GameOptions::NUM_ROUNDS;
}


void Game::askMove() {
	std::cout << "\nIt is player " << (currentPlayer + 1) << "\'s turn.\n\n"
			  << "Pick a move:\n (1) Skip\n"
			  << (discard.empty() ? " (-) Draw From Discard\n"
								  : " (2) Draw From Discard\n")
			  << (deck.empty() ? " (-) Draw From Deck\n"
							   : " (3) Draw From Deck\n");

get_move:
	switch (getConsoleInput<std::size_t>()) {
		case 1:
			players[currentPlayer].skip();
			break;
		case 2:
			if (discard.empty()) {
				std::cout << "Discard is empty.\n";
				goto get_move;
			}
			players[currentPlayer].drawFromDiscard(discard);
			break;
		case 3:
			if (deck.empty()) {
				std::cout << "Deck is empty.\n";
				goto get_move;
			}
			players[currentPlayer].drawFromDeck(deck);
			discard.push_front(deck.back());
			deck.pop_back();
			break;
		default:
			std::cout << "Input must be an element of [1, 3]" << '\n';
			goto get_move;
			break;
	}
}


void Game::showCards() {
	std::cout << "Deck: ";
	if (deck.empty()) {
		std::cout << "--";
	} else {
		std::cout << deck.back();
	}

	std::cout << "\nDiscard: ";
	if (discard.empty()) {
		std::cout << "--";
	} else {
		std::cout << discard.front();
	}
	std::cout << '\n';
}


void Game::displayScores() const {
	std::cout << "Scores\n";
	for (std::size_t i = 0; i < maxPlayers; ++i) {
		std::cout << "Player " << (i + 1) << ": " << players[i].getScore()
				  << '\n';
	}
}


std::size_t Game::findWinner() const {
	std::size_t winner = 0;
	for (std::size_t i = 0; i < maxPlayers; ++i) {
		if (players[winner].getScore() < players[i].getScore()) {
			winner = i;
		}
	}

	// Return 0 if tie
	for (std::size_t i = 0; i < maxPlayers; ++i) {
		if ((winner != i) &&
			(players[winner].getScore() == players[i].getScore())) {
			return 0;
		}
	}

	return winner + 1;
}


/*** Non-Member Functions *****************************************************/
bool askPlaying() {
	char choice;
	std::cout << "Do you want to play again? [Y/N]" << std::endl;
get_playing:
	choice = std::tolower(getConsoleInput<char>());
	if (!(choice == 'y' || choice == 'n')) {
		std::cout << "Input must be Y or N.\n";
		goto get_playing;
	}

	return choice == 'y';
}

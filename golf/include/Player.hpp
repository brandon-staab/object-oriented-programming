/**
 *  \brief		Project: Golf
 *  \details	Implementation of Golf card game
 *  \author		Brandon Staab <bls114@zips.uakron.edu>
 *  \version	1.0.0
 *  \date		October 2017
 *  \copyright	COPYRIGHT (C) 2017 Brandon Staab (bls114) All rights reserved.
 */

#ifndef GUARD_Player
#define GUARD_Player

#include "Card.hpp"
#include "Hand.hpp"
#include "Discard.hpp"

#include <memory>
#include <stack>


struct Player {
	~Player();

	void peak();
	int getScore() const;
	void setCards(Hand& hand);

	/*** Turn Types ***********************************************************/
	void skip();
	void drawFromDeck(Deck& deck);
	void drawFromDiscard(Discard& discard);

   private:
	PlayingCard* cards[4];
};


/*** Non-Member Fnctions ******************************************************/
std::size_t getCardSwap();

#endif

/**
 *  \brief		Project: Golf
 *  \details	Implementation of Golf card game
 *  \author		Brandon Staab <bls114@zips.uakron.edu>
 *  \version	1.0.0
 *  \date		October 2017
 *  \copyright	COPYRIGHT (C) 2017 Brandon Staab (bls114) All rights reserved.
 */

#ifndef GUARD_GameOptions
#define GUARD_GameOptions


struct GameOptions {
	GameOptions() = delete;

	static const int NUM_ROUNDS = 9;
	static const int NUM_CARDS_PER_PLAYER = 4;
};

#endif

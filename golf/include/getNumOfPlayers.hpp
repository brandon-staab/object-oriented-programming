/**
 *  \brief		Project: Golf
 *  \details	Implementation of Golf card game
 *  \author		Brandon Staab <bls114@zips.uakron.edu>
 *  \version	1.0.0
 *  \date		October 2017
 *  \copyright	COPYRIGHT (C) 2017 Brandon Staab (bls114) All rights reserved.
 */

#ifndef GUARD_getNumOfPlayers
#define GUARD_getNumOfPlayers

#include <cstddef>


std::size_t getNumOfPlayers();

#endif

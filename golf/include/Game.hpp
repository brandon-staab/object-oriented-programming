/**
 *  \brief		Project: Golf
 *  \details	Implementation of Golf card game
 *  \author		Brandon Staab <bls114@zips.uakron.edu>
 *  \version	1.0.0
 *  \date		October 2017
 *  \copyright	COPYRIGHT (C) 2017 Brandon Staab (bls114) All rights reserved.
 */

#ifndef GUARD_Game
#define GUARD_Game

#include "Player.hpp"

#include "Deck.hpp"

#include <memory>
#include <stack>


class Game {
   public:
	Game(const std::size_t numOfPlayers = 2);
	~Game();

	void reset();
	int run();
	void step();
	void play();
	void round();
	void turn();

	bool isPlaying() const;
	bool gameOver() const;

	void askMove();
	void showCards();

	void displayScores() const;
	std::size_t findWinner() const;

   private:
	bool playing;
	std::size_t rounds;
	std::size_t currentPlayer;
	std::size_t maxPlayers;
	Deck deck;
	Discard discard;
	std::unique_ptr<Player[]> players;
};


/*** Non-Member Functions *****************************************************/
bool askPlaying();

#endif

/**
 *  \brief		Project 1: Postfix
 *  \details	Data Structures project for Dr. C.-C. Chan
 *  \author		Brandon Staab <bls114@zips.uakron.edu>
 *  \version	1.0.0
 *  \date		October 2017
 *  \copyright	COPYRIGHT (C) 2017 Brandon Staab (bls114) All rights reserved.
 */

#include "getConsoleInput.hpp"

#include <iostream>
#include <limits>
#include <string>
#include <typeinfo>


/*** Compiler Manged Names ***/
const std::string MANGLED_NAME_BOOL{"b"};
const std::string MANGLED_NAME_CHAR{"b"};
const std::string MANGLED_NAME_INT{"i"};
const std::string MANGLED_NAME_SIZE_T{"m"};
const std::string MANGLED_NAME_STRING{
	"NSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE"};


/*** Explicit instantiation definition  ***************************************/
template bool getConsoleInput<bool>();
template char getConsoleInput<char>();
template int getConsoleInput<int>();
template std::size_t getConsoleInput<std::size_t>();


namespace {
template <typename T>
std::string getType(T&) {
	std::string typeID = typeid(T).name();

	if (typeID == MANGLED_NAME_BOOL) {
		return "bit";
	} else if (typeID == MANGLED_NAME_CHAR) {
		return "letter";
	} else if (typeID == MANGLED_NAME_INT) {
		return "number";
	} else if (typeID == MANGLED_NAME_SIZE_T) {
		return "positive number";
	} else if (typeID == MANGLED_NAME_STRING) {
		return "string";
	} else {
		return typeID;
	}
}
}


template <typename T>
T getConsoleInput() {
	bool valid = false;
	T value;

	do {
		std::cout << "> ";
		std::cin >> value;

		if (!std::cin.good()) {
			std::cin.clear();
			std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');

			std::cerr << "Error: bad input" << std::endl;
			std::cout << "Enter a(n) " << getType(value) << ":" << std::endl;
		} else {
			break;
		}
	} while (!valid);

	return value;
}


template <>
std::string getConsoleInput() {
	bool valid = false;
	std::string value;

	do {
		std::cout << "> ";

		std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
		std::getline(std::cin, value);

		if (!std::cin.good() || value.empty()) {
			std::cin.clear();
			std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');

			std::cerr << "Error: bad input" << std::endl;
			std::cout << "Enter a(n) " << getType(value) << ":" << std::endl;
		} else {
			break;
		}
	} while (!valid);

	return value;
}

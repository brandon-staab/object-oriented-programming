/**
 *  \brief		getRandEngine() function
 *  \details	This is part of the utility library
 *  \author		Brandon Staab <bls114@zips.uakron.edu>
 *  \version	1.0.0
 *  \date		September 2017
 *  \copyright	COPYRIGHT (C) 2017 Brandon Staab (bls114) All rights reserved.
 */

#include "getRandEngine.hpp"


#if GEN_MODE

namespace {
std::random_device rng{};
std::mt19937_64 generator{rng()};
}

std::mt19937_64& getRandEngine() {
	return generator;
}

#else

namespace {
std::random_device rng{};
std::default_random_engine generator{rng()};
}

std::default_random_engine& getRandEngine() {
	return generator;
}

#endif

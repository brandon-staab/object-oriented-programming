/**
 *  \brief		getRandEngine() function
 *  \details	This is part of the utility library
 *  \author		Brandon Staab <bls114@zips.uakron.edu>
 *  \version	1.0.0
 *  \date		September 2017
 *  \copyright	COPYRIGHT (C) 2017 Brandon Staab (bls114) All rights reserved.
 */

#ifndef GUARD_getRandEngine
#define GUARD_getRandEngine
#define GEN_MODE 0

#include <random>

#if GEN_MODE
std::mt19937_64& getRandEngine();
#else
std::default_random_engine& getRandEngine();
#endif


#endif
